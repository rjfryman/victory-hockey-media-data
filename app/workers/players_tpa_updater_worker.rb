class PlayersTpaUpdaterWorker
  include Sidekiq::Worker

  def perform()
    Skater.all.each do |skater|
      skater.update(tpa: skater.skater_stat.tpa)
    end
    Goalie.all.each do |goalie|
      goalie.update(tpa: goalie.goalie_stat.tpa)
    end
  end
end
