class TeamPlayersRetrievalWorker
  include Sidekiq::Worker
  require 'open-uri'

  def perform(id)
    team = Team.find(id)
    href = team.href
    url = "https://vhlportal.com#{href}"
    doc = Nokogiri::HTML(open(url))
    doc.css("#drafts table").each do |table|
      if table.css('thead').text.include?('Player')
        table.css('tr').each do |row|
          player_info = row.css('th').text.squish.split(" ")
          href = row.css('th a').attribute('href').value
          player_draft_season = player_info.shift
          player_position = player_info.shift
          player_info.shift
          player_name = player_info.join(' ')
          draft_season = DraftSeason.find_or_create_by(name: player_draft_season)
          position = Position.find_or_create_by(name: player_position)
          skater_attributes_hash = {}
          skater_attributes = [:ck, :fg, :di, :sk, :st, :ph, :fo, :pa, :sc, :df, :ps, :ex, :ld, :tpa]
          skater_attributes.each_with_index do |sa, index|
            data = row.css('td')[index].text.to_i
            skater_attributes_hash.merge!(sa => data)
          end
          skater = Skater.find_or_create_by(name: player_name)
          if skater.skater_stat
            id = skater.skater_stat_id
            skater.update(skater_stat_id: nil)
            SkaterStat.delete(id)
          end
          player_stats = SkaterStat.create(skater_attributes_hash)
          skater.update(href: href, draft_season: draft_season, team: team,  position: position, skater_stat: player_stats)
          SkaterGetVitalsWorker.perform_async(skater.id)
          # SkaterTpaRetrivalWorker.perform_in(5.minutes, skater.id)
        end
      end
      if table.css('thead').text.include?('Goalie')
        table.css('tr').each do |row|
          player_info = row.css('th').text.squish.split(" ")
          href = row.css('th a').attribute('href').value
          player_draft_season = player_info.shift
          player_position = player_info.shift
          player_info.shift
          player_name = player_info.join(' ')
          draft_season = DraftSeason.find_or_create_by(name: player_draft_season)
          position = Position.find_or_create_by(name: player_position)
          goalie_attributes_hash = {}
          goalie_attributes = [:sk, :sz, :ag, :rb, :sc, :hs, :rt, :ph, :ps, :ex, :ld, :tpa]
          goalie_attributes.each_with_index do |ga, index|
            data = row.css('td')[index].text.to_i
            goalie_attributes_hash.merge!(ga => data)
          end
          goalie = Goalie.find_or_create_by(name: player_name)
          if goalie.goalie_stat
            id = goalie.goalie_stat_id
            goalie.update(goalie_stat_id: nil)
            GoalieStat.delete(id)
          end
          goalie_stats = GoalieStat.create(goalie_attributes_hash)
          goalie.update(href: href, draft_season: draft_season, team: team, position: position, goalie_stat: goalie_stats)
          GoalieGetVitalsWorker.perform_async(goalie.id)
          # GoalieTpaRetrivalWorker.perform_in(5.minutes, goalie.id)
        end
      end
    end
  end
end

