class SkaterTpaRetrivalWorker
  include Sidekiq::Worker

  def perform(id)
    Capybara.register_driver :poltergeist do |app|
      Capybara::Poltergeist::Driver.new(app, {
          js_errors: false,
          timeout: 1000,
          phantomjs_options: [
              '--load-images=no',
              '--ignore-ssl-errors=yes',
              '--ssl-protocol=any']})
    end
    player = Skater.find id
    session = Capybara::Session.new(:poltergeist)
    href = player.href
    url = "https://vhlportal.com#{href}"
    session.visit(url)
    session.click_link 'Update Log'
    session.select('All', :from => "Show", visible: false)
    session.all('#UpdateLog tbody .group-end').each do |group_end|
      data = group_end.all('td')
      date = Date.parse data[0].text
      tpe = data[1].text
      SkaterTpeUpdate.find_or_create_by(skater: player, week_ending: date).update(tpe: tpe)
    end
  end
end
