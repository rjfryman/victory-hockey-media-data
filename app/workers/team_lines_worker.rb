class TeamLinesWorker
  include Sidekiq::Worker
  require 'open-uri'

  def perform(id)
    team = Team.find(id)
    href = team.simulation_url
    url = "https://vhlportal.com#{href}"
    doc = doc = Nokogiri::HTML(open(url))
    doc.css("#tabmain4 .STHSPHPTeamStat_Table").each do |table|
      case table.children.first.text
        when "5 vs 5 Forward"
          five_five_forward table, team
        when "5 vs 5 Defense"
          five_five_defense table, team
        when "Power Play Forward"
          power_play_forward table, team
        when "Power Play Defense"
          power_play_defense table, team
        when "Penalty Kill 4 Players Forward"
          penalty_kill_four_forward table, team
        when "Penalty Kill 4 Players Defense"
          penalty_kill_four_defense table, team
        when "Penalty Kill 3 Players"
          penalty_kill_three_players table, team
        when "4 vs 4 Forward"
          four_four_forward table, team
        when "4 vs 4 Defense"
          four_four_defense table, team
        when "Last Minutes Offensive"
          last_min_offensive table, team
        when "Last Minutes Defensive"
          last_min_defensive table, team
        else
          puts table.children.first.text
      end
    end
  end
end

def five_five_forward table, team
  table.css('tr')[2..5].each do |line|
    line_number = line.children[0].text
    left_wing = Skater.find_by_name(line.children[1].text) || Skater.find_by_name("Bot")
    center = Skater.find_by_name(line.children[2].text) || Skater.find_by_name("Bot")
    right_wing = Skater.find_by_name(line.children[3].text) || Skater.find_by_name("Bot")
    time_percentage = line.children[4].text
    phy = line.children[5].text
    df = line.children[6].text
    of = line.children[7].text
    total_tpa = left_wing.tpa + center.tpa + right_wing.tpa
    FiveVsFiveForward.find_or_create_by(team: team, line_number: line_number).update(total_tpa: total_tpa, left_wing: left_wing, center: center, right_wing: right_wing, time_percentage: time_percentage, phy: phy, df: df, of: of)
  end
end

def five_five_defense table, team
  table.css('tr')[2..5].each do |line|
    line_number = line.children[0].text
    left_defense = Skater.find_by_name(line.children[1].text) || Skater.find_by_name("Bot")
    right_defense = Skater.find_by_name(line.children[2].text) || Skater.find_by_name("Bot")
    time_percentage = line.children[4].text
    phy = line.children[5].text
    df = line.children[6].text
    of = line.children[7].text
    total_tpa = left_defense.tpa + right_defense.tpa
    FiveVsFiveDefense.find_or_create_by(team: team, line_number: line_number).update(total_tpa: total_tpa,left_defense: left_defense, right_defense: right_defense, time_percentage: time_percentage, phy: phy, df: df, of: of)
  end
end

def power_play_forward table, team
  table.css('tr')[2..3].each do |line|
    line_number = line.children[0].text
    left_wing = Skater.find_by_name(line.children[1].text) || Skater.find_by_name("Bot")
    center = Skater.find_by_name(line.children[2].text) || Skater.find_by_name("Bot")
    right_wing = Skater.find_by_name(line.children[3].text) || Skater.find_by_name("Bot")
    time_percentage = line.children[4].text
    phy = line.children[5].text
    df = line.children[6].text
    of = line.children[7].text
    total_tpa = left_wing.tpa + center.tpa + right_wing.tpa
    PowerPlayForward.find_or_create_by(team: team, line_number: line_number).update(total_tpa: total_tpa, left_wing: left_wing, center: center, right_wing: right_wing, time_percentage: time_percentage, phy: phy, df: df, of: of)
  end
end

def power_play_defense table, team
  table.css('tr')[2..3].each do |line|
    line_number = line.children[0].text
    left_defense = Skater.find_by_name(line.children[1].text) || Skater.find_by_name("Bot")
    right_defense = Skater.find_by_name(line.children[2].text) || Skater.find_by_name("Bot")
    time_percentage = line.children[4].text
    phy = line.children[5].text
    df = line.children[6].text
    of = line.children[7].text
    total_tpa = left_defense.tpa + right_defense.tpa
    PowerPlayDefense.find_or_create_by(team: team, line_number: line_number).update(total_tpa: total_tpa, left_defense: left_defense, right_defense: right_defense, time_percentage: time_percentage, phy: phy, df: df, of: of)
  end
end

def penalty_kill_four_forward table, team
  table.css('tr')[2..5].each do |line|
    line_number = line.children[0].text
    winger = Skater.find_by_name(line.children[1].text) || Skater.find_by_name("Bot")
    center = Skater.find_by_name(line.children[2].text) || Skater.find_by_name("Bot")
    time_percentage = line.children[3].text
    phy = line.children[4].text
    df = line.children[5].text
    of = line.children[6].text
    total_tpa = winger.tpa + center.tpa
    PenaltyKillFourPlayersForward.find_or_create_by(team: team, line_number: line_number).update(total_tpa: total_tpa, winger: winger, center: center, time_percentage: time_percentage, phy: phy, df: df, of: of)
  end
end

def penalty_kill_four_defense table, team
  table.css('tr')[2..3].each do |line|
    line_number = line.children[0].text
    left_defense = Skater.find_by_name(line.children[1].text) || Skater.find_by_name("Bot")
    right_defense = Skater.find_by_name(line.children[2].text) || Skater.find_by_name("Bot")
    time_percentage = line.children[3].text
    phy = line.children[4].text
    df = line.children[5].text
    of = line.children[6].text
    total_tpa = left_defense.tpa + right_defense.tpa
    PenaltyKillFourPlayersDefense.find_or_create_by(team: team, line_number: line_number).update(total_tpa: total_tpa, left_defense: left_defense, right_defense: right_defense, time_percentage: time_percentage, phy: phy, df: df, of: of)
  end
end

def penalty_kill_three_players table, team
  table.css('tr')[2..3].each do |line|
    line_number = line.children[0].text
    winger = Skater.find_by_name(line.children[1].text) || Skater.find_by_name("Bot")
    winger_time_percentage = line.children[2].text
    winger_phy = line.children[3].text
    winger_df = line.children[4].text
    winger_of = line.children[5].text
    left_defense = Skater.find_by_name(line.children[6].text) || Skater.find_by_name("Bot")
    right_defense = Skater.find_by_name(line.children[7].text) || Skater.find_by_name("Bot")
    defense_time_percentage = line.children[8].text
    defense_phy = line.children[9].text
    defense_df = line.children[10].text
    defense_of = line.children[11].text
    total_tpa = left_defense.tpa + right_defense.tpa + winger.tpa
    PenaltyKillThreePlayer.find_or_create_by(team: team, line_number: line_number).update(total_tpa: total_tpa, winger: winger, winger_time_percentage: winger_time_percentage, winger_phy: winger_phy, winger_df: winger_df, winger_of: winger_of,
                                                                                     left_defense: left_defense, right_defense: right_defense, defense_time_percentage: defense_time_percentage, defense_phy: defense_phy,
                                                                                     defense_df: defense_df, defense_of: defense_of)
  end
end

def four_four_forward table, team
  table.css('tr')[2..5].each do |line|
    line_number = line.children[0].text
    winger = Skater.find_by_name(line.children[1].text) || Skater.find_by_name("Bot")
    center = Skater.find_by_name(line.children[2].text) || Skater.find_by_name("Bot")
    time_percentage = line.children[3].text
    phy = line.children[4].text
    df = line.children[5].text
    of = line.children[6].text
    total_tpa = winger.tpa + center.tpa
    FourVsFourForward.find_or_create_by(team: team, line_number: line_number).update(total_tpa: total_tpa, winger: winger, center: center, time_percentage: time_percentage, phy: phy, df: df, of: of)
  end
end

def four_four_defense table, team
  table.css('tr')[2..3].each do |line|
    line_number = line.children[0].text
    left_defense = Skater.find_by_name(line.children[1].text) || Skater.find_by_name("Bot")
    right_defense = Skater.find_by_name(line.children[2].text) || Skater.find_by_name("Bot")
    time_percentage = line.children[3].text
    phy = line.children[4].text
    df = line.children[5].text
    of = line.children[6].text
    total_tpa = left_defense.tpa + right_defense.tpa
    FourVsFourDefense.find_or_create_by(team: team, line_number: line_number).update(total_tpa: total_tpa, left_defense: left_defense, right_defense: right_defense, time_percentage: time_percentage, phy: phy, df: df, of: of)
  end
end

def last_min_offensive table, team
  line = table.css('tr')[2]
  left_wing = Skater.find_by_name(line.children[0].text) || Skater.find_by_name("Bot")
  center = Skater.find_by_name(line.children[1].text) || Skater.find_by_name("Bot")
  right_wing = Skater.find_by_name(line.children[2].text) || Skater.find_by_name("Bot")
  left_defense = Skater.find_by_name(line.children[3].text) || Skater.find_by_name("Bot")
  right_defense = Skater.find_by_name(line.children[4].text) || Skater.find_by_name("Bot")
  total_tpa = left_wing.tpa + center.tpa + right_wing.tpa + left_defense.tpa + right_defense.tpa
  LastMinuteAttack.find_or_create_by(team: team).update(total_tpa: total_tpa, left_wing: left_wing, center: center, right_wing: right_wing, left_defense: left_defense, right_defense: right_defense)
end

def last_min_defensive table, team
  line = table.css('tr')[2]
  left_wing = Skater.find_by_name(line.children[0].text) || Skater.find_by_name("Bot")
  center = Skater.find_by_name(line.children[1].text) || Skater.find_by_name("Bot")
  right_wing = Skater.find_by_name(line.children[2].text) || Skater.find_by_name("Bot")
  left_defense = Skater.find_by_name(line.children[3].text) || Skater.find_by_name("Bot")
  right_defense = Skater.find_by_name(line.children[4].text) || Skater.find_by_name("Bot")
  total_tpa = left_wing.tpa + center.tpa + right_wing.tpa + left_defense.tpa + right_defense.tpa
  LastMinuteDefend.find_or_create_by(team: team).update(total_tpa: total_tpa, left_wing: left_wing, center: center, right_wing: right_wing, left_defense: left_defense, right_defense: right_defense)
end