class SkaterGetVitalsWorker
  include Sidekiq::Worker
  require 'open-uri'

  def perform(id)
    skater = Skater.find(id)
    href = skater.href
    url = "https://vhlportal.com#{href}"
    doc = Nokogiri::HTML(open(url))
    vitals = doc.css(".vitals")
    height = vitals[3].text
    weight = vitals[4].text
    age = vitals[5].text.strip.split(" ").last
    country_abbr = vitals[6].children.attr('src').value.strip.split('/').last.strip.split('.').first
    country = Country.find_or_create_by(abbr: country_abbr)
    skater.update(height: height, weight: weight, age: age, country: country)
  end
end
