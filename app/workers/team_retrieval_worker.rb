class TeamRetrievalWorker
  include Sidekiq::Worker
  require 'open-uri'

  def perform()
    url = 'https://vhlportal.com/teams'
    doc = doc = Nokogiri::HTML(open(url))
    vhl = League.find_or_create_by(name: 'VHL')
    vhlm = League.find_or_create_by(name: 'VHLM')
    [{name: 'vhl', league: vhl}, {name: 'vhlm', league: vhlm}].each do |league|
      doc.css("##{league[:name]} .card").each do |card|
        href = card.at_css('a').attributes["href"].value
        name = card.at_css('.card-body h5').text
        team = Team.find_or_create_by(name: name, href: href, league: league[:league])
        TeamPlayersRetrievalWorker.perform_async(team.id)
        TeamLinesWorker.perform_in(1.minutes, team.id)
      end
    end
    PlayersTpaUpdaterWorker.perform_in(1.minutes)
  end
end
