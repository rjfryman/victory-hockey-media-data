class GoaliesController < ApplicationController
  before_action :set_goalie, only: [:show, :edit, :update, :destroy, :get_tpa_updates]

  # GET /goalies
  # GET /goalies.json
  def index
    @goalies = Goalie.all
  end

  # GET /goalies/1
  # GET /goalies/1.json
  def show
    @goalie_stat = @goalie.goalie_stat
  end

  # GET /goalies/new
  def new
    @goalie = Goalie.new
  end

  # GET /goalies/1/edit
  def edit
  end

  # POST /goalies
  # POST /goalies.json
  def create
    @goalie = goalie.new(goalie_params)

    respond_to do |format|
      if @goalie.save
        format.html { redirect_to @goalie, notice: 'goalie was successfully created.' }
        format.json { render :show, status: :created, location: @goalie }
      else
        format.html { render :new }
        format.json { render json: @goalie.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /goalies/1
  # PATCH/PUT /goalies/1.json
  def update
    respond_to do |format|
      if @goalie.update(goalie_params)
        format.html { redirect_to @goalie, notice: 'goalie was successfully updated.' }
        format.json { render :show, status: :ok, location: @goalie }
      else
        format.html { render :edit }
        format.json { render json: @goalie.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /goalies/1
  # DELETE /goalies/1.json
  def destroy
    @goalie.destroy
    respond_to do |format|
      format.html { redirect_to goalies_url, notice: 'goalie was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def get_tpa_updates
    GoalieTpaRetrivalWorker.perform_async @goalie.id
    redirect_to @goalie
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_goalie
      @goalie = Goalie.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def goalie_params
      params.require(:goalie).permit(:name, :href, :position_id, :team_id, :draft_season_id, :goalie_stat_id)
    end
end
