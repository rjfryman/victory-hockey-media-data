class TpeUpdatesController < ApplicationController
  before_action :set_tpe_update, only: [:show, :edit, :update, :destroy]

  # GET /tpe_updates
  # GET /tpe_updates.json
  def index
    @tpe_updates = TpeUpdate.all
  end

  # GET /tpe_updates/1
  # GET /tpe_updates/1.json
  def show
  end

  # GET /tpe_updates/new
  def new
    @tpe_update = TpeUpdate.new
  end

  # GET /tpe_updates/1/edit
  def edit
  end

  # POST /tpe_updates
  # POST /tpe_updates.json
  def create
    @tpe_update = TpeUpdate.new(tpe_update_params)

    respond_to do |format|
      if @tpe_update.save
        format.html { redirect_to @tpe_update, notice: 'Tpe update was successfully created.' }
        format.json { render :show, status: :created, location: @tpe_update }
      else
        format.html { render :new }
        format.json { render json: @tpe_update.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /tpe_updates/1
  # PATCH/PUT /tpe_updates/1.json
  def update
    respond_to do |format|
      if @tpe_update.update(tpe_update_params)
        format.html { redirect_to @tpe_update, notice: 'Tpe update was successfully updated.' }
        format.json { render :show, status: :ok, location: @tpe_update }
      else
        format.html { render :edit }
        format.json { render json: @tpe_update.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /tpe_updates/1
  # DELETE /tpe_updates/1.json
  def destroy
    @tpe_update.destroy
    respond_to do |format|
      format.html { redirect_to tpe_updates_url, notice: 'Tpe update was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_tpe_update
      @tpe_update = TpeUpdate.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def tpe_update_params
      params.require(:tpe_update).permit(:tpe, :week_ending)
    end
end
