class DraftSeasonsController < ApplicationController
  before_action :set_draft_season, only: [:show, :edit, :update, :destroy]

  # GET /draft_seasons
  # GET /draft_seasons.json
  def index
    @draft_seasons = DraftSeason.all
  end

  # GET /draft_seasons/1
  # GET /draft_seasons/1.json
  def show
  end

  # GET /draft_seasons/new
  def new
    @draft_season = DraftSeason.new
  end

  # GET /draft_seasons/1/edit
  def edit
  end

  # POST /draft_seasons
  # POST /draft_seasons.json
  def create
    @draft_season = DraftSeason.new(draft_season_params)

    respond_to do |format|
      if @draft_season.save
        format.html { redirect_to @draft_season, notice: 'Draft season was successfully created.' }
        format.json { render :show, status: :created, location: @draft_season }
      else
        format.html { render :new }
        format.json { render json: @draft_season.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /draft_seasons/1
  # PATCH/PUT /draft_seasons/1.json
  def update
    respond_to do |format|
      if @draft_season.update(draft_season_params)
        format.html { redirect_to @draft_season, notice: 'Draft season was successfully updated.' }
        format.json { render :show, status: :ok, location: @draft_season }
      else
        format.html { render :edit }
        format.json { render json: @draft_season.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /draft_seasons/1
  # DELETE /draft_seasons/1.json
  def destroy
    @draft_season.destroy
    respond_to do |format|
      format.html { redirect_to draft_seasons_url, notice: 'Draft season was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_draft_season
      @draft_season = DraftSeason.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def draft_season_params
      params.require(:draft_season).permit(:name)
    end
end
