class GoalieStatsController < ApplicationController
  before_action :set_goalie_stat, only: [:show, :edit, :update, :destroy]

  # GET /goalie_stats
  # GET /goalie_stats.json
  def index
    @goalie_stats = GoalieStat.all
  end

  # GET /goalie_stats/1
  # GET /goalie_stats/1.json
  def show
  end

  # GET /goalie_stats/new
  def new
    @goalie_stat = GoalieStat.new
  end

  # GET /goalie_stats/1/edit
  def edit
  end

  # POST /goalie_stats
  # POST /goalie_stats.json
  def create
    @goalie_stat = GoalieStat.new(goalie_stat_params)

    respond_to do |format|
      if @goalie_stat.save
        format.html { redirect_to @goalie_stat, notice: 'Goalie stat was successfully created.' }
        format.json { render :show, status: :created, location: @goalie_stat }
      else
        format.html { render :new }
        format.json { render json: @goalie_stat.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /goalie_stats/1
  # PATCH/PUT /goalie_stats/1.json
  def update
    respond_to do |format|
      if @goalie_stat.update(goalie_stat_params)
        format.html { redirect_to @goalie_stat, notice: 'Goalie stat was successfully updated.' }
        format.json { render :show, status: :ok, location: @goalie_stat }
      else
        format.html { render :edit }
        format.json { render json: @goalie_stat.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /goalie_stats/1
  # DELETE /goalie_stats/1.json
  def destroy
    @goalie_stat.destroy
    respond_to do |format|
      format.html { redirect_to goalie_stats_url, notice: 'Goalie stat was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_goalie_stat
      @goalie_stat = GoalieStat.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def goalie_stat_params
      params.require(:goalie_stat).permit(:sk, :sz, :ag, :rb, :sc, :hs, :rt, :ph, :ps, :ex, :ld, :tpa)
    end
end
