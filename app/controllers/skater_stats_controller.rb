class SkaterStatsController < ApplicationController
  before_action :set_skater_stat, only: [:show, :edit, :update, :destroy]

  # GET /skater_stats
  # GET /skater_stats.json
  def index
    @skater_stats = SkaterStat.all
  end

  # GET /skater_stats/1
  # GET /skater_stats/1.json
  def show
  end

  # GET /skater_stats/new
  def new
    @skater_stat = SkaterStat.new
  end

  # GET /skater_stats/1/edit
  def edit
  end

  # POST /skater_stats
  # POST /skater_stats.json
  def create
    @skater_stat = SkaterStat.new(skater_stat_params)

    respond_to do |format|
      if @skater_stat.save
        format.html { redirect_to @skater_stat, notice: 'Skater stat was successfully created.' }
        format.json { render :show, status: :created, location: @skater_stat }
      else
        format.html { render :new }
        format.json { render json: @skater_stat.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /skater_stats/1
  # PATCH/PUT /skater_stats/1.json
  def update
    respond_to do |format|
      if @skater_stat.update(skater_stat_params)
        format.html { redirect_to @skater_stat, notice: 'Skater stat was successfully updated.' }
        format.json { render :show, status: :ok, location: @skater_stat }
      else
        format.html { render :edit }
        format.json { render json: @skater_stat.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /skater_stats/1
  # DELETE /skater_stats/1.json
  def destroy
    @skater_stat.destroy
    respond_to do |format|
      format.html { redirect_to skater_stats_url, notice: 'Skater stat was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_skater_stat
      @skater_stat = SkaterStat.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def skater_stat_params
      params.require(:skater_stat).permit(:ck, :sk, :fg, :di, :st, :ph, :fo, :pa, :sc, :df, :ps, :ex, :ld, :tpa)
    end
end
