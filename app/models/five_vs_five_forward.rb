class FiveVsFiveForward < ApplicationRecord
  belongs_to :team
  belongs_to :left_wing, class_name: :Skater, foreign_key: :left_wing_id, optional: true
  belongs_to :center, class_name: :Skater, foreign_key: :center_id, optional: true
  belongs_to :right_wing, class_name: :Skater, foreign_key: :right_wing_id, optional: true
end