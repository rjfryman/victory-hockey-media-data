class Goalie < ApplicationRecord
  belongs_to :position, required: false
  belongs_to :team, required: false
  belongs_to :draft_season, required: false
  belongs_to :goalie_stat, required: false
  belongs_to :country, required: false
  has_many :goalie_tpe_updates, -> { order(week_ending: :desc)}
end
