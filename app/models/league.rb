class League < ApplicationRecord
  validates :name, uniqueness: true
  validates :name, presence: true
  has_many :teams
end
