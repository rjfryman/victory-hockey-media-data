class Skater < ApplicationRecord
  belongs_to :position, required: false
  belongs_to :team, required: false
  belongs_to :draft_season, required: false
  belongs_to :skater_stat, required: false
  belongs_to :country, required: false
  has_many :skater_tpe_updates, -> { order(week_ending: :desc)}
end
