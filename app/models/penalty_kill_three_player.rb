class PenaltyKillThreePlayer < ApplicationRecord
  belongs_to :team
  belongs_to :winger, class_name: :Skater, foreign_key: :winger_id, optional: true
  belongs_to :left_defense, class_name: :Skater, foreign_key: :left_defense_id, optional: true
  belongs_to :right_defense, class_name: :Skater, foreign_key: :right_defense_id, optional: true
end
