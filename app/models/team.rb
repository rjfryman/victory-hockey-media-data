class Team < ApplicationRecord
  validates :name, uniqueness: true
  validates :name, presence: true
  belongs_to :league
  has_many :skaters, -> { order(position_id: :asc, tpa: :desc) }
  has_many :goalies, -> { order(tpa: :desc) }
  has_many :skater_stats, through: :skaters
  has_many :goalie_stats, through: :goalies
  has_many :five_vs_five_forwards, -> { order(line_number: :asc) }
  has_many :five_vs_five_defenses, -> { order(line_number: :asc) }
  has_many :four_vs_four_forwards, -> { order(line_number: :asc) }
  has_many :four_vs_four_defenses, -> { order(line_number: :asc) }
  has_many :last_minute_attacks
  has_many :last_minute_defends
  has_many :penalty_kill_four_players_forwards, -> { order(line_number: :asc) }
  has_many :penalty_kill_four_players_defenses, -> { order(line_number: :asc) }
  has_many :penalty_kill_three_players, -> { order(line_number: :asc) }
  has_many :power_play_forwards, -> { order(line_number: :asc) }
  has_many :power_play_defenses, -> { order(line_number: :asc) }


  def centers
    skaters.where(position_id: 1)
  end

  def center_total_tpa
    centers.sum(:tpa)
  end

  def center_avg_tpa
    (center_total_tpa / centers.count) unless centers.count == 0
  end

  def left_wingers
    skaters.where(position_id: 2)
  end

  def left_wingers_total_tpa
    left_wingers.sum(:tpa)
  end

  def left_wingers_avg_tpa
    (left_wingers_total_tpa / left_wingers.count) unless left_wingers.count == 0
  end

  def right_wingers
    skaters.where(position_id: 3)
  end

  def right_wingers_total_tpa
    right_wingers.sum(:tpa)
  end

  def right_wingers_avg_tpa
    (right_wingers_total_tpa / right_wingers.count) unless right_wingers.count == 0
  end

  def defenders
    skaters.where(position_id: 4)
  end

  def defenders_total_tpa
    defenders.sum(:tpa)
  end

  def defenders_avg_tpa
    (defenders_total_tpa / defenders.count) unless defenders.count == 0
  end

  def total_team_tpa
    total_skater_tpa + total_goalie_tpa
  end

  def avg_tpa
    (total_tpa / skaters.count + goalies.count) unless skaters.count == 0 or goalies.count == 0
  end

  def total_skater_tpa
    skater_stats.sum(:tpa)
  end

  def total_forward_tpa
    tpa = 0
    skaters.where(position_id: [1..3]).each do |skater|
      tpa += skater.skater_stat.tpa
    end
    tpa
  end

  def avg_forward_tpa
    (total_forward_tpa / skaters.where(position_id: [1..3]).count) unless skaters.where(position_id: [1..3]).count == 0
  end

  def total_defender_tpa
    tpa = 0
    skaters.where(position_id: 4).each do |skater|
      tpa += skater.skater_stat.tpa
    end
    tpa
  end

  def avg_defender_tpa
    (total_defender_tpa / skaters.where(position_id: 4).count) unless skaters.where(position_id: 4).count == 0
  end

  def total_goalie_tpa
    goalie_stats.sum(:tpa)
  end

  def avg_goalie_tpa
    (goalie_stats.sum(:tpa) / goalie_stats.count) unless goalie_stats.count == 0
  end
end
