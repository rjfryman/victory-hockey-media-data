class FourVsFourForward < ApplicationRecord
  belongs_to :team
  belongs_to :winger, class_name: :Skater, foreign_key: :winger_id, optional: true
  belongs_to :center, class_name: :Skater, foreign_key: :center_id, optional: true
end
