class Country < ApplicationRecord
  has_many :skaters
  has_many :goalies
end
