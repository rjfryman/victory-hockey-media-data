json.extract! goalie_stat, :id, :sk, :sz, :ag, :rb, :sc, :hs, :rt, :ph, :ex, :ld, :tpa, :created_at, :updated_at
json.url goalie_stat_url(goalie_stat, format: :json)
