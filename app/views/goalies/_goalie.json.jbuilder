json.extract! goalie, :id, :name, :href, :position_id, :team_id, :draft_season_id, :goalie_stat_id, :created_at, :updated_at
json.url goalie_url(goalie, format: :json)
