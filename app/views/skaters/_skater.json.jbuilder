json.extract! skater, :id, :name, :href, :position_id, :team_id, :draft_season_id, :skater_stat_id, :created_at, :updated_at
json.url skater_url(skater, format: :json)
