json.extract! skater_stat, :id, :sk, :fg, :di, :st, :ph, :fo, :pa, :sc, :df, :ps, :ex, :ld, :tpa, :created_at, :updated_at
json.url skater_stat_url(skater_stat, format: :json)
