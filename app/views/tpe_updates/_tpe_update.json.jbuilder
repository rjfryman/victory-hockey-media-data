json.extract! tpe_update, :id, :tpe, :week_ending, :created_at, :updated_at
json.url tpe_update_url(tpe_update, format: :json)
