json.extract! draft_season, :id, :name, :created_at, :updated_at
json.url draft_season_url(draft_season, format: :json)
