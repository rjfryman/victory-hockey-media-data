# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_03_18_143550) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "countries", force: :cascade do |t|
    t.string "name"
    t.string "abbr"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "draft_seasons", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "five_vs_five_defenses", force: :cascade do |t|
    t.bigint "team_id"
    t.string "line_number"
    t.bigint "left_defense_id"
    t.bigint "right_defense_id"
    t.string "time_percentage"
    t.integer "phy"
    t.integer "df"
    t.integer "of"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "total_tpa"
    t.index ["left_defense_id"], name: "index_five_vs_five_defenses_on_left_defense_id"
    t.index ["right_defense_id"], name: "index_five_vs_five_defenses_on_right_defense_id"
    t.index ["team_id"], name: "index_five_vs_five_defenses_on_team_id"
  end

  create_table "five_vs_five_forwards", force: :cascade do |t|
    t.bigint "team_id"
    t.string "line_number"
    t.bigint "left_wing_id"
    t.bigint "center_id"
    t.bigint "right_wing_id"
    t.string "time_percentage"
    t.integer "phy"
    t.integer "df"
    t.integer "of"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "total_tpa"
    t.index ["center_id"], name: "index_five_vs_five_forwards_on_center_id"
    t.index ["left_wing_id"], name: "index_five_vs_five_forwards_on_left_wing_id"
    t.index ["right_wing_id"], name: "index_five_vs_five_forwards_on_right_wing_id"
    t.index ["team_id"], name: "index_five_vs_five_forwards_on_team_id"
  end

  create_table "four_vs_four_defenses", force: :cascade do |t|
    t.bigint "team_id"
    t.string "line_number"
    t.bigint "left_defense_id"
    t.bigint "right_defense_id"
    t.string "time_percentage"
    t.integer "phy"
    t.integer "df"
    t.integer "of"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "total_tpa"
    t.index ["left_defense_id"], name: "index_four_vs_four_defenses_on_left_defense_id"
    t.index ["right_defense_id"], name: "index_four_vs_four_defenses_on_right_defense_id"
    t.index ["team_id"], name: "index_four_vs_four_defenses_on_team_id"
  end

  create_table "four_vs_four_forwards", force: :cascade do |t|
    t.bigint "team_id"
    t.string "line_number"
    t.bigint "winger_id"
    t.bigint "center_id"
    t.string "time_percentage"
    t.integer "phy"
    t.integer "df"
    t.integer "of"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "total_tpa"
    t.index ["center_id"], name: "index_four_vs_four_forwards_on_center_id"
    t.index ["team_id"], name: "index_four_vs_four_forwards_on_team_id"
    t.index ["winger_id"], name: "index_four_vs_four_forwards_on_winger_id"
  end

  create_table "goalie_stats", force: :cascade do |t|
    t.integer "sk"
    t.integer "sz"
    t.integer "ag"
    t.integer "rb"
    t.integer "sc"
    t.integer "hs"
    t.integer "rt"
    t.integer "ph"
    t.integer "ps"
    t.integer "ex"
    t.integer "ld"
    t.integer "tpa"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "goalie_tpe_updates", force: :cascade do |t|
    t.integer "tpe"
    t.date "week_ending"
    t.bigint "goalie_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["goalie_id"], name: "index_goalie_tpe_updates_on_goalie_id"
  end

  create_table "goalies", force: :cascade do |t|
    t.string "name"
    t.string "href"
    t.bigint "position_id"
    t.bigint "team_id"
    t.bigint "draft_season_id"
    t.bigint "goalie_stat_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "tpa"
    t.bigint "country_id"
    t.string "height"
    t.string "weight"
    t.string "age"
    t.index ["country_id"], name: "index_goalies_on_country_id"
    t.index ["draft_season_id"], name: "index_goalies_on_draft_season_id"
    t.index ["goalie_stat_id"], name: "index_goalies_on_goalie_stat_id"
    t.index ["position_id"], name: "index_goalies_on_position_id"
    t.index ["team_id"], name: "index_goalies_on_team_id"
  end

  create_table "last_minute_attacks", force: :cascade do |t|
    t.bigint "team_id"
    t.bigint "left_wing_id"
    t.bigint "center_id"
    t.bigint "right_wing_id"
    t.bigint "left_defense_id"
    t.bigint "right_defense_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "total_tpa"
    t.index ["center_id"], name: "index_last_minute_attacks_on_center_id"
    t.index ["left_defense_id"], name: "index_last_minute_attacks_on_left_defense_id"
    t.index ["left_wing_id"], name: "index_last_minute_attacks_on_left_wing_id"
    t.index ["right_defense_id"], name: "index_last_minute_attacks_on_right_defense_id"
    t.index ["right_wing_id"], name: "index_last_minute_attacks_on_right_wing_id"
    t.index ["team_id"], name: "index_last_minute_attacks_on_team_id"
  end

  create_table "last_minute_defends", force: :cascade do |t|
    t.bigint "team_id"
    t.bigint "left_wing_id"
    t.bigint "center_id"
    t.bigint "right_wing_id"
    t.bigint "left_defense_id"
    t.bigint "right_defense_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "total_tpa"
    t.index ["center_id"], name: "index_last_minute_defends_on_center_id"
    t.index ["left_defense_id"], name: "index_last_minute_defends_on_left_defense_id"
    t.index ["left_wing_id"], name: "index_last_minute_defends_on_left_wing_id"
    t.index ["right_defense_id"], name: "index_last_minute_defends_on_right_defense_id"
    t.index ["right_wing_id"], name: "index_last_minute_defends_on_right_wing_id"
    t.index ["team_id"], name: "index_last_minute_defends_on_team_id"
  end

  create_table "last_updates", force: :cascade do |t|
    t.string "name"
    t.datetime "time"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "leagues", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "penalty_kill_four_players_defenses", force: :cascade do |t|
    t.bigint "team_id"
    t.string "line_number"
    t.bigint "left_defense_id"
    t.bigint "right_defense_id"
    t.string "time_percentage"
    t.integer "phy"
    t.integer "df"
    t.integer "of"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "total_tpa"
    t.index ["left_defense_id"], name: "index_penalty_kill_four_players_defenses_on_left_defense_id"
    t.index ["right_defense_id"], name: "index_penalty_kill_four_players_defenses_on_right_defense_id"
    t.index ["team_id"], name: "index_penalty_kill_four_players_defenses_on_team_id"
  end

  create_table "penalty_kill_four_players_forwards", force: :cascade do |t|
    t.bigint "team_id"
    t.string "line_number"
    t.bigint "center_id"
    t.bigint "winger_id"
    t.string "time_percentage"
    t.integer "phy"
    t.integer "df"
    t.integer "of"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "total_tpa"
    t.index ["center_id"], name: "index_penalty_kill_four_players_forwards_on_center_id"
    t.index ["team_id"], name: "index_penalty_kill_four_players_forwards_on_team_id"
    t.index ["winger_id"], name: "index_penalty_kill_four_players_forwards_on_winger_id"
  end

  create_table "penalty_kill_three_players", force: :cascade do |t|
    t.bigint "team_id"
    t.string "line_number"
    t.bigint "winger_id"
    t.bigint "left_defense_id"
    t.bigint "right_defense_id"
    t.string "winger_time_percentage"
    t.integer "winger_phy"
    t.integer "winger_df"
    t.integer "winger_of"
    t.string "defense_time_percentage"
    t.integer "defense_phy"
    t.integer "defense_df"
    t.integer "defense_of"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "total_tpa"
    t.index ["left_defense_id"], name: "index_penalty_kill_three_players_on_left_defense_id"
    t.index ["right_defense_id"], name: "index_penalty_kill_three_players_on_right_defense_id"
    t.index ["team_id"], name: "index_penalty_kill_three_players_on_team_id"
    t.index ["winger_id"], name: "index_penalty_kill_three_players_on_winger_id"
  end

  create_table "positions", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "power_play_defenses", force: :cascade do |t|
    t.bigint "team_id"
    t.string "line_number"
    t.bigint "left_defense_id"
    t.bigint "right_defense_id"
    t.string "time_percentage"
    t.integer "phy"
    t.integer "df"
    t.integer "of"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "total_tpa"
    t.index ["left_defense_id"], name: "index_power_play_defenses_on_left_defense_id"
    t.index ["right_defense_id"], name: "index_power_play_defenses_on_right_defense_id"
    t.index ["team_id"], name: "index_power_play_defenses_on_team_id"
  end

  create_table "power_play_forwards", force: :cascade do |t|
    t.bigint "team_id"
    t.string "line_number"
    t.bigint "left_wing_id"
    t.bigint "center_id"
    t.bigint "right_wing_id"
    t.string "time_percentage"
    t.integer "phy"
    t.integer "df"
    t.integer "of"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "total_tpa"
    t.index ["center_id"], name: "index_power_play_forwards_on_center_id"
    t.index ["left_wing_id"], name: "index_power_play_forwards_on_left_wing_id"
    t.index ["right_wing_id"], name: "index_power_play_forwards_on_right_wing_id"
    t.index ["team_id"], name: "index_power_play_forwards_on_team_id"
  end

  create_table "skater_stats", force: :cascade do |t|
    t.integer "sk"
    t.integer "fg"
    t.integer "di"
    t.integer "st"
    t.integer "ph"
    t.integer "fo"
    t.integer "pa"
    t.integer "sc"
    t.integer "df"
    t.integer "ps"
    t.integer "ex"
    t.integer "ld"
    t.integer "tpa"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "ck"
  end

  create_table "skater_tpe_updates", force: :cascade do |t|
    t.integer "tpe"
    t.date "week_ending"
    t.bigint "skater_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["skater_id"], name: "index_skater_tpe_updates_on_skater_id"
  end

  create_table "skaters", force: :cascade do |t|
    t.string "name"
    t.string "href"
    t.bigint "position_id"
    t.bigint "team_id"
    t.bigint "draft_season_id"
    t.bigint "skater_stat_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "tpa"
    t.string "height"
    t.string "weight"
    t.string "age"
    t.bigint "country_id"
    t.index ["country_id"], name: "index_skaters_on_country_id"
    t.index ["draft_season_id"], name: "index_skaters_on_draft_season_id"
    t.index ["position_id"], name: "index_skaters_on_position_id"
    t.index ["skater_stat_id"], name: "index_skaters_on_skater_stat_id"
    t.index ["team_id"], name: "index_skaters_on_team_id"
  end

  create_table "teams", force: :cascade do |t|
    t.string "name"
    t.string "href"
    t.bigint "league_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "simulation_url"
    t.index ["league_id"], name: "index_teams_on_league_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  add_foreign_key "five_vs_five_defenses", "skaters", column: "left_defense_id"
  add_foreign_key "five_vs_five_defenses", "skaters", column: "right_defense_id"
  add_foreign_key "five_vs_five_forwards", "skaters", column: "center_id"
  add_foreign_key "five_vs_five_forwards", "skaters", column: "left_wing_id"
  add_foreign_key "five_vs_five_forwards", "skaters", column: "right_wing_id"
  add_foreign_key "four_vs_four_defenses", "skaters", column: "left_defense_id"
  add_foreign_key "four_vs_four_defenses", "skaters", column: "right_defense_id"
  add_foreign_key "four_vs_four_forwards", "skaters", column: "center_id"
  add_foreign_key "four_vs_four_forwards", "skaters", column: "winger_id"
  add_foreign_key "goalies", "draft_seasons"
  add_foreign_key "goalies", "goalie_stats"
  add_foreign_key "goalies", "positions"
  add_foreign_key "goalies", "teams"
  add_foreign_key "last_minute_attacks", "skaters", column: "center_id"
  add_foreign_key "last_minute_attacks", "skaters", column: "left_defense_id"
  add_foreign_key "last_minute_attacks", "skaters", column: "left_wing_id"
  add_foreign_key "last_minute_attacks", "skaters", column: "right_defense_id"
  add_foreign_key "last_minute_attacks", "skaters", column: "right_wing_id"
  add_foreign_key "last_minute_defends", "skaters", column: "center_id"
  add_foreign_key "last_minute_defends", "skaters", column: "left_defense_id"
  add_foreign_key "last_minute_defends", "skaters", column: "left_wing_id"
  add_foreign_key "last_minute_defends", "skaters", column: "right_defense_id"
  add_foreign_key "last_minute_defends", "skaters", column: "right_wing_id"
  add_foreign_key "penalty_kill_four_players_defenses", "skaters", column: "left_defense_id"
  add_foreign_key "penalty_kill_four_players_defenses", "skaters", column: "right_defense_id"
  add_foreign_key "penalty_kill_four_players_forwards", "skaters", column: "center_id"
  add_foreign_key "penalty_kill_four_players_forwards", "skaters", column: "winger_id"
  add_foreign_key "penalty_kill_three_players", "skaters", column: "left_defense_id"
  add_foreign_key "penalty_kill_three_players", "skaters", column: "right_defense_id"
  add_foreign_key "penalty_kill_three_players", "skaters", column: "winger_id"
  add_foreign_key "power_play_defenses", "skaters", column: "left_defense_id"
  add_foreign_key "power_play_defenses", "skaters", column: "right_defense_id"
  add_foreign_key "power_play_forwards", "skaters", column: "center_id"
  add_foreign_key "power_play_forwards", "skaters", column: "left_wing_id"
  add_foreign_key "power_play_forwards", "skaters", column: "right_wing_id"
  add_foreign_key "skaters", "draft_seasons"
  add_foreign_key "skaters", "positions"
  add_foreign_key "skaters", "skater_stats"
  add_foreign_key "skaters", "teams"
end
