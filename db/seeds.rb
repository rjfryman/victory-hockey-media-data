Position.create(name: 'C')
Position.create(name: 'LW')
Position.create(name: 'RW')
Position.create(name: 'D')
Position.create(name: 'G')
stats = SkaterStat.create(sk: 20, fg: 20, di: 20, st: 20, ph: 20, fo: 20, pa: 20, sc: 20, df: 20, ps: 20, ex: 20, ld: 20, tpa: 0)
Skater.create(name: 'Bot', skater_stat: stats, position: Position.first)


countries = {"Afghanistan"=>"AF", "Anguilla"=>"AI", "Armenia"=>"AM", "Antarctica"=>"AQ", "American Samoa"=>"AS", "Austria"=>"AT", "Australia"=>"AU", "Bangladesh"=>"BD", "Belgium"=>"BE", "Bulgaria"=>"BG", "Bahrain"=>"BH", "Brazil"=>"BR", "Canada"=>"CA", "Congo"=>"CG", "Switzerland"=>"CH", "Cook Islands"=>"CK", "China"=>"CN", "Cuba"=>"CU", "Christmas Island"=>"CX", "Czechia"=>"CZ", "Germany"=>"DE", "Djibouti"=>"DJ", "Denmark"=>"DK", "Dominican Republic"=>"DO", "Estonia"=>"EE", "Egypt"=>"EG", "Spain"=>"ES", "Finland"=>"FI", "Faroe Islands"=>"FO", "France"=>"FR", "United Kingdom"=>"GB", "Greece"=>"GR", "Hong Kong"=>"HK", "Honduras"=>"HN", "Haiti"=>"HT", "Hungary"=>"HU", "Ireland"=>"IE", "Israel"=>"IL", "Isle of Man"=>"IM", "India"=>"IN", "Iran"=>"IR", "Iceland"=>"IS", "Italy"=>"IT", "Jamaica"=>"JM", "Japan"=>"JP", "Kyrgyzstan"=>"KG", "Cambodia"=>"KH", "Saint Kitts and Nevis"=>"KN", "North Korea"=>"KP", "South Korea"=>"KR", "Cayman Islands"=>"KY", "Kazakhstan"=>"KZ", "Liechtenstein"=>"LI", "Sri Lanka"=>"LK", "Lithuania"=>"LT", "Latvia"=>"LV", "Morocco"=>"MA", "Mongolia"=>"MN", "Mexico"=>"MX", "Nigeria"=>"NG", "Netherlands"=>"NL", "Norway"=>"NO", "New Zealand"=>"NZ", "Pakistan"=>"PK", "Poland"=>"PL", "Puerto Rico"=>"PR", "Portugal"=>"PT", "Romania"=>"RO", "Serbia"=>"RS", "Russia"=>"RU", "Sweden"=>"SE", "Singapore"=>"SG", "Slovenia"=>"SI", "Slovakia"=>"SK", "Sierra Leone"=>"SL", "Togo"=>"TG", "Taiwan"=>"TW", "Tanzania"=>"TZ", "Ukraine"=>"UA", "United States of America"=>"US", "Virgin Islands (U.S.)"=>"VI", "Vietnam"=>"VN", "Wallis and Futuna"=>"WF", "South Africa"=>"ZA", "Zimbabwe"=>"ZW"}
countries.each do |c|
  Country.find_or_create_by(name: c.first, abbr: c.last)
end