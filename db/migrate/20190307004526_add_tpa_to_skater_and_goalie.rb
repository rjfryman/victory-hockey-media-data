class AddTpaToSkaterAndGoalie < ActiveRecord::Migration[5.2]
  def change
    add_column :skaters, :tpa, :integer
    add_column :goalies, :tpa, :integer
  end
end
