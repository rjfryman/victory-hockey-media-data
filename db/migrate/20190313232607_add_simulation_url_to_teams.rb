class AddSimulationUrlToTeams < ActiveRecord::Migration[5.2]
  def change
    add_column :teams, :simulation_url, :string
  end
end
