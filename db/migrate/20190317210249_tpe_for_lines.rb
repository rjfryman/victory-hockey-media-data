class TpeForLines < ActiveRecord::Migration[5.2]
  def change
    add_column :five_vs_five_forwards, :total_tpa, :integer
    add_column :five_vs_five_defenses, :total_tpa, :integer
    add_column :power_play_forwards, :total_tpa, :integer
    add_column :power_play_defenses, :total_tpa, :integer
    add_column :penalty_kill_four_players_forwards, :total_tpa, :integer
    add_column :penalty_kill_four_players_defenses, :total_tpa, :integer
    add_column :penalty_kill_three_players, :total_tpa, :integer
    add_column :four_vs_four_forwards, :total_tpa, :integer
    add_column :four_vs_four_defenses, :total_tpa, :integer
    add_column :last_minute_attacks, :total_tpa, :integer
    add_column :last_minute_defends, :total_tpa, :integer
  end
end
