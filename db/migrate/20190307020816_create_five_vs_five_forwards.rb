class CreateFiveVsFiveForwards < ActiveRecord::Migration[5.2]
  def change
    create_table :five_vs_five_forwards do |t|
      t.references :team
      t.string :line_number
      t.references :left_wing, index: true, foreign_key: {to_table: :skaters}
      t.references :center, index: true, foreign_key: {to_table: :skaters}
      t.references :right_wing, index: true, foreign_key: {to_table: :skaters}
      t.string :time_percentage
      t.integer :phy
      t.integer :df
      t.integer :of
      t.timestamps
    end
  end
end
