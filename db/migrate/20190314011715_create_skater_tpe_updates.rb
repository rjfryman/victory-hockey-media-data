class CreateSkaterTpeUpdates < ActiveRecord::Migration[5.2]
  def change
    create_table :skater_tpe_updates do |t|
      t.integer :tpe
      t.date :week_ending
      t.references :skater

      t.timestamps
    end
  end
end
