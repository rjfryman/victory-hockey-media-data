class CreateFourVsFourForwards < ActiveRecord::Migration[5.2]
  def change
    create_table :four_vs_four_forwards do |t|
      t.references :team
      t.string :line_number
      t.references :winger, index: true, foreign_key: {to_table: :skaters}
      t.references :center, index: true, foreign_key: {to_table: :skaters}
      t.string :time_percentage
      t.integer :phy
      t.integer :df
      t.integer :of
      t.timestamps
    end
  end
end
