class CreatePenaltyKillThreePlayers < ActiveRecord::Migration[5.2]
  def change
    create_table :penalty_kill_three_players do |t|
      t.references :team
      t.string :line_number
      t.references :winger, index: true, foreign_key: {to_table: :skaters}
      t.references :left_defense, index: true, foreign_key: {to_table: :skaters}
      t.references :right_defense, index: true, foreign_key: {to_table: :skaters}
      t.string :winger_time_percentage
      t.integer :winger_phy
      t.integer :winger_df
      t.integer :winger_of
      t.string :defense_time_percentage
      t.integer :defense_phy
      t.integer :defense_df
      t.integer :defense_of
      t.timestamps
    end
  end
end
