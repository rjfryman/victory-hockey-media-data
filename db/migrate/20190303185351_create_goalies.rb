class CreateGoalies < ActiveRecord::Migration[5.2]
  def change
    create_table :goalies do |t|
      t.string :name
      t.string :href
      t.references :position, foreign_key: true
      t.references :team, foreign_key: true
      t.references :draft_season, foreign_key: true
      t.references :goalie_stat, foreign_key: true

      t.timestamps
    end
  end
end
