class CreateGoalieTpeUpdates < ActiveRecord::Migration[5.2]
  def change
    create_table :goalie_tpe_updates do |t|
      t.integer :tpe
      t.date :week_ending
      t.references :goalie

      t.timestamps
    end
  end
end
