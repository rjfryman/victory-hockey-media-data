class AddCountryToSkatersAndGoalies < ActiveRecord::Migration[5.2]
  def change
    add_reference :skaters, :country, index: true
    add_reference :goalies, :country, index: true
  end
end
