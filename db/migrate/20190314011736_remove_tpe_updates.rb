class RemoveTpeUpdates < ActiveRecord::Migration[5.2]
  def change
    drop_table :tpe_updates
  end
end
