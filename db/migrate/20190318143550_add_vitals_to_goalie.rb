class AddVitalsToGoalie < ActiveRecord::Migration[5.2]
  def change
    add_column :goalies, :height, :string
    add_column :goalies, :weight, :string
    add_column :goalies, :age, :string
  end
end
