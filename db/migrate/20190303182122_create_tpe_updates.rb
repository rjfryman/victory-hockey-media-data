class CreateTpeUpdates < ActiveRecord::Migration[5.2]
  def change
    create_table :tpe_updates do |t|
      t.integer :tpe
      t.date :week_ending

      t.timestamps
    end
  end
end
