class CreateLastUpdates < ActiveRecord::Migration[5.2]
  def change
    create_table :last_updates do |t|
      t.string :name
      t.datetime :time

      t.timestamps
    end
  end
end
