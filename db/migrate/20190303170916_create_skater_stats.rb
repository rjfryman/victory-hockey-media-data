class CreateSkaterStats < ActiveRecord::Migration[5.2]
  def change
    create_table :skater_stats do |t|
      t.integer :sk
      t.integer :fg
      t.integer :di
      t.integer :sk
      t.integer :st
      t.integer :ph
      t.integer :fo
      t.integer :pa
      t.integer :sc
      t.integer :df
      t.integer :ps
      t.integer :ex
      t.integer :ld
      t.integer :tpa

      t.timestamps
    end
  end
end
