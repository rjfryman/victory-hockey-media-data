class AddVitalsToSkater < ActiveRecord::Migration[5.2]
  def change
    add_column :skaters, :height, :string
    add_column :skaters, :weight, :string
    add_column :skaters, :age, :string
  end
end
