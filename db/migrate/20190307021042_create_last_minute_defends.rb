class CreateLastMinuteDefends < ActiveRecord::Migration[5.2]
  def change
    create_table :last_minute_defends do |t|
      t.references :team
      t.references :left_wing, index: true, foreign_key: {to_table: :skaters}
      t.references :center, index: true, foreign_key: {to_table: :skaters}
      t.references :right_wing, index: true, foreign_key: {to_table: :skaters}
      t.references :left_defense, index: true, foreign_key: {to_table: :skaters}
      t.references :right_defense, index: true, foreign_key: {to_table: :skaters}
      t.timestamps
    end
  end
end
