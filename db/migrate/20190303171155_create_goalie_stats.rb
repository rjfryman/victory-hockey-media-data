class CreateGoalieStats < ActiveRecord::Migration[5.2]
  def change
    create_table :goalie_stats do |t|
      t.integer :sk
      t.integer :sz
      t.integer :ag
      t.integer :rb
      t.integer :sc
      t.integer :hs
      t.integer :rt
      t.integer :ph
      t.integer :ps
      t.integer :ex
      t.integer :ld
      t.integer :tpa

      t.timestamps
    end
  end
end
