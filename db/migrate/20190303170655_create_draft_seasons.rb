class CreateDraftSeasons < ActiveRecord::Migration[5.2]
  def change
    create_table :draft_seasons do |t|
      t.string :name

      t.timestamps
    end
  end
end
