require 'sidekiq/web'

Rails.application.routes.draw do
  resources :countries
  resources :tpe_updates
  resources :teams do
    member do
      post 'get_lines_update'
    end
  end
  resources :skaters do
    member do
      post 'get_tpa_updates'
    end
  end
  resources :skater_stats
  resources :positions
  resources :leagues
  resources :goalies do
    member do
      post 'get_tpa_updates'
    end
  end
  resources :goalie_stats
  resources :draft_seasons
  devise_for :users

  post 'league/data_update', to: 'leagues#update_data', as: :update_data

  root 'leagues#index'
  mount Sidekiq::Web => '/sidekiq'
end
